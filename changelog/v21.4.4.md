# Changelog

All notable changes to this project will be documented in this file.

## [21.4.4] - 2022-02-22

## Changed
* Change GVM_RUN_DIR to GSAD_RUN_DIR, GVMD_RUN_DIR [7ecbe29a5](https://github.com/greenbone/gsad/commit/7ecbe29a5)
* Use full path GSAD_PID_PATH for PID files [70792699d](https://github.com/greenbone/gsad/commit/70792699d)

[21.4.4]: https://github.com/greenbone/gsad/compare/21.4.4...21.4.4